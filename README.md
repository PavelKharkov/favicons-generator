# Favicons-generator

## Начало работы:
```
yarn install
```

**Для генерации файла favicon.ico с разрешением 32x32 необходимо подправить файл по пути node_modules/favicons/dist/config/icons.json. Там удалить по пути "favicons" -> "favicon.ico" элементы массива иконок с размерами 48x48 и 64x64**

Положите файл фавиконки с названием favicon.svg или favicon.png в папку input\
После этого запустите:
```
yarn generate
```

Сгенерированные иконки появятся в папке output
