'use strict';

const fs = require('fs');
const path = require('path');

const favicons = require('favicons');

const printTime = date => {
    return `[${String(date.getHours()).padStart(2, '0')}:` +
        `${String(date.getMinutes()).padStart(2, '0')}:` +
        `${String(date.getSeconds()).padStart(2, '0')}]`;
};

const startTime = new Date();
console.log(`${printTime(startTime)} Starting...`);

const encoding = 'utf8';
const inputDir = 'input';
const outputDir = 'output';

const svgFavicon = path.join(inputDir, 'favicon.svg');
const faviconsIcon = fs.existsSync(svgFavicon) ? svgFavicon : path.join(inputDir, 'favicon.png');
if (!fs.existsSync(faviconsIcon)) return console.log('Favicon file doesn\'t exists');

const faviconsConfig = {
    path: '/',
    appName: null,
    appShortName: null,
    appDescription: null,
    developerName: 'Internet studio 100up.ru',
    developerURL: 'info@100up.ru',
    dir: 'auto',
    lang: 'ru-RU',
    background: '#fff',
    theme_color: '#fff',
    appleStatusBarStyle: 'black-translucent',
    display: 'browser',
    orientation: 'any',
    scope: '/',
    start_url: '/',
    version: '1.0',
    logging: false,
    pixel_art: false,
    loadManifestWithCredentials: false,
    icons: {
        android: true,
        appleIcon: true,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        windows: false,
        yandex: false
    }
};
const faviconsCallback = (error, response) => {
    if (error) return console.log(error.message);

    if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

    response.files.forEach(file => {
        fs.writeFile(path.join(outputDir, file.name), file.contents, encoding, fileError => {
            if (fileError) throw error;
        });
    });
    response.images.forEach(file => {
        fs.writeFile(path.join(outputDir, file.name), file.contents, encoding, fileError => {
            if (fileError) throw error;
        });
    });

    console.log(response.html);

    const endTime = new Date();
    console.log(`${printTime(endTime)} Finished after ${endTime.getTime() - startTime.getTime()} ms`);
};

favicons(faviconsIcon, faviconsConfig, faviconsCallback);